/* @flow */

import React from 'react';
import createRootNavigator from './routes/createRootNavigator';
import {connect} from 'react-redux';
import {IS_AUTH} from './data/actions/authActions';

type Action = {
  type: string,
  [string]: mixed,
};

type Props = {
  isAuth: boolean,
  dispatch: Action => void,
};

function mapStateToProps(state) {
  let {isAuth} = state.auth;
  return {
    isAuth,
  };
}

function Authenticator(props: Props) {
  let {isAuth, dispatch} = props;
  dispatch({type: IS_AUTH});
  let Layout = createRootNavigator(isAuth);
  return React.createElement(Layout);
}

export default connect(mapStateToProps)(Authenticator);
