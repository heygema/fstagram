import {createStore, combineReducers, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './data/sagas/rootSaga';
import authReducer from './data/reducers/authReducer';

const sagaMiddleware = createSagaMiddleware();
const app = combineReducers({
  auth: authReducer,
});

export default createStore(app, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);
