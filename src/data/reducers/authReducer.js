// @flow
import {SIGN_IN, SIGN_OUT} from '../actions/authActions';

let authState = {
  isAuth: false,
};

export default function authReducer(state = authState, action) {
  switch (action.type) {
    case SIGN_IN: {
      return {...state, isAuth: true};
    }
    case SIGN_OUT: {
      return {...state, isAuth: false};
    }
    default:
      return state;
  }
}
