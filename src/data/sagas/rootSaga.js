// @flow
import {fork} from 'redux-saga/effects';
import type {Saga} from 'redux-saga';

import {authSagaWatcher} from './authSagas';

export default function* rootSaga(): Saga<void> {
  yield fork(authSagaWatcher);
}
