// @flow

import {takeEvery, call, put} from 'redux-saga/effects';
import {SIGN_IN, SIGN_OUT, IS_AUTH} from '../actions/authActions';
import type {Saga} from 'redux-saga';
import {AsyncStorage} from 'react-native';

function* loginSaga(): Saga<void> {
  try {
    yield call(AsyncStorage.setItem, 'LOCAL_TOKEN', 'safd3248239rew89y');
  } catch (e) {
    throw new Error('Error login saga');
  }
}

function* logoutSaga(): Saga<void> {
  try {
    yield call(AsyncStorage.removeItem, 'LOCAL_TOKEN');
  } catch (e) {
    throw new Error('Error logout saga');
  }
}

function* loadFromStorage(): Saga<void> {
  try {
    let tokenAvailable = yield call(AsyncStorage.getItem, 'LOCAL_TOKEN');
    if (tokenAvailable) {
      yield put({type: SIGN_IN});
    }
  } catch (e) {
    throw new Error('Error load from storage saga');
  }
}

function* authSagaWatcher() {
  yield takeEvery(SIGN_IN, loginSaga);
  yield takeEvery(SIGN_OUT, logoutSaga);
  yield takeEvery(IS_AUTH, loadFromStorage);
}

export {authSagaWatcher};
