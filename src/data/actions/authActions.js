export const SIGN_IN = 'SIGN_IN';
export const IS_AUTH = 'IS_AUTH';
export const SIGN_OUT = 'SIGN_OUT';

export function signIn() {
  return {
    type: SIGN_IN,
  };
}

export function signOut() {
  return {
    type: SIGN_OUT,
  };
}

export function isAuth() {
  return {
    type: IS_AUTH,
  };
}
