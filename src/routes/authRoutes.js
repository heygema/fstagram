// @flow
import {TabNavigator, StackNavigator} from 'react-navigation';
import React from 'react';
import {Header, Body, Title} from 'native-base';
import Home from '../scenes/App/Home';
import Profile from '../scenes/App/Profile';

type HeaderProps = {
  title: string,
};

const HeadComponent = (props: HeaderProps) => {
  return (
    <Header style={{paddingTop: 10}}>
      <Body>
        <Title>{props.title} dong</Title>
      </Body>
    </Header>
  );
};

const Tab = TabNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        title: 'Home',
      },
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        title: 'Profile',
      },
    },
  },
  {
    navigationOptions: {
      swipeEnabled: true,
    },
  },
);

export default StackNavigator(
  {
    Default: {
      screen: Tab,
    },
  },
  {
    navigationOptions: ({navigation}) => {
      const {index, routes} = navigation.state;
      let name = routes[index].routeName;
      return {
        header: <HeadComponent title={name} />,
      };
    },
  },
);
