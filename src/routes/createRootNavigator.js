// @flow
import {StackNavigator} from 'react-navigation';
import authRoutes from './authRoutes';
import unauthRoutes from './unauthRoutes';

export default function createRootNavigator(isAuth = false) {
  return StackNavigator(
    {
      Unauth: {
        screen: unauthRoutes,
        navigationOptions: {
          gestureEnabled: false,
        },
      },
      Auth: {
        screen: authRoutes,
        navigationOptions: {
          gestureEnabled: false,
        },
      },
    },
    {
      headerMode: 'none',
      mode: 'modal',
      initialRouteName: isAuth ? 'Auth' : 'Unauth',
    },
  );
}
