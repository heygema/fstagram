import {TabNavigator, StackNavigator} from 'react-navigation';
import Signin from '../scenes/Auth/Signin';
import Signup from '../scenes/Auth/Signup';

const Screen = TabNavigator({
  SignIn: {
    screen: Signin,
    navigationOptions: () => {
      return {
        title: 'sign in',
      };
    },
  },
  SignUp: {
    screen: Signup,
    navigationOptions: () => {
      return {
        title: 'sign up',
      };
    },
  },
});

export default StackNavigator({
  Default: {
    screen: Screen,
  },
});
