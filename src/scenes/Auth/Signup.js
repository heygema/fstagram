/* @flow */

import React, {Component} from 'react';
import {Container, Content, Button, Form, Item, Input, Text} from 'native-base';
export default class Signup extends Component {
  render() {
    return (
      <Container>
        <Content>
          <Form>
            <Item>
              <Input placeholder="Username" />
            </Item>
            <Item>
              <Input placeholder="Password" />
            </Item>
            <Item last>
              <Input placeholder="Confirm Password" />
            </Item>

            <Button primary>
              <Text>Sign up</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}
