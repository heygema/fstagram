/* @flow */

import React, {Component} from 'react';
import {Container, Content, Button, Form, Item, Input, Text} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {signIn} from '../../data/actions/authActions';

function mdtp(dispatch) {
  return {
    actions: bindActionCreators({signIn}, dispatch),
  };
}

type Props = {
  actions: {
    [string]: () => void,
  },
};

@connect(null, mdtp)
export default class Signin extends Component<Props, {}> {
  render() {
    let {actions: {signIn}} = this.props;

    return (
      <Container>
        <Content>
          <Form>
            <Item>
              <Input placeholder="Username" />
            </Item>
            <Item last>
              <Input placeholder="Password" />
            </Item>
            <Button onPress={signIn} primary>
              <Text>Sign in</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}
