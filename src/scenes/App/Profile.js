/* @flow */

import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Button, Text as TextNative} from 'native-base';
import {connect} from 'react-redux';

type Props = {
  dispatch: () => void,
};

@connect()
export default class Profile extends Component<Props, {}> {
  render() {
    let {dispatch} = this.props;
    return (
      <View style={styles.container}>
        <Button
          onPress={() =>
            dispatch({
              type: 'SIGN_OUT',
            })
          }
        >
          <TextNative>Signout here</TextNative>
        </Button>
        <Text>{"I'm the Profile component"}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
